"use strict";
console.log("App.js loaded");

const app = angular
    .module("app", ["ngRoute"])
    .controller("AppCtrl", AppCtrl)
    .factory("breed", function($http) {
        console.log("Factory initized");
        var data = {};

        $http.get("https://dog.ceo/api/breeds/list/all")
            .then(function(response) {
                data = response.data.message;
                console.log("Data: ====> ", data);
            }).catch(function(error) {
                console.log("Error: ", error);
            });

        return {
            getData: function() {
                return data;
            }
        }
    });

function AppCtrl(breed) {
    var vm = this;
    vm.breed = breed.getData();
    vm.title = "Danniel";
    console.log("====> VM ", vm);
    console.log("====> Breed: ", breed);
};

// ROUTER
app.config(["$routeProvider", function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "./app.html",
            controller: "AppCtrl"
        });
}]);
